General Information
-------------------
STT -  Sanity Test tool

This module helps configuring - Sanity test screnario's which are must to RUN after any production deployment.
UI of the configuration page can help setting up Sanity test cases easily.
Drag Drop feature and Simple UI will make it easy for CMS manager configured Sanity test cases.

Internally - It will use simpletest features but will be more user friendly and easy to set up.

Downloads
---------
TBD


Installation and Usage
----------------------
STT will used Simpletest feature internally will have dependacy for the same.


Drupal Module Author
--------------------
Swapnil P Sarvankar (SwapS)
https://www.drupal.org/user/1488880
